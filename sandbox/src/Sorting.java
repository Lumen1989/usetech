import java.io.File;
import java.io.IOException;
import java.util.Scanner;

import static java.util.Arrays.sort;

public class Sorting {
    public static void main(String[] args) throws IOException {
        File file = new File("src/unsortedNumbers.txt");

        //считываем строку из указанного файла
        Scanner scanner = new Scanner(file);
        String line = scanner.nextLine();

        //получаем каждое число в отдельности
        String[] numbersString = line.split(",");

        int[] numbers = new int[21];
        int counter = 0;

        // в цикле foreach преобразовываем каждое значение в int
        for (String number : numbersString) {
            numbers[counter++] = Integer.parseInt(number);
        }
        // сортируем массив
        sort(numbers);

        // вывод значений массива по возрастанию
        for (int i = 0; i < numbers.length; i++) {
            System.out.print(numbers[i] + " ");
        }

        System.out.println();

        // вывод значений массива по убыванию
        for (int i = numbers.length - 1; i > 0; i--) {
            System.out.print(numbers[i] + " ");
        }
        scanner.close();
    }
}
